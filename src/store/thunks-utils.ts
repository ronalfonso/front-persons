import {helpFetch} from '../core/helpers/helpFetch';


export const getList = async (url: string) => {
    return helpFetch().get(url).then(resp => {
        return resp;
    })
        .catch(err => console.log(err));
}

export const create = async (data: any, url: string) => {
    try {
        return await helpFetch().post(url, {data})
            .then(resp => resp);
    } catch (err: any) {
        const {data} = err.response;
        return data;
    }
}
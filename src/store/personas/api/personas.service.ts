import {create} from "../../thunks-utils";
import {helpFetch} from "../../../core/helpers/helpFetch";
import {ResponseRequest} from "../../../core/models/ResponseRequest";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const URL_COMPONENT = `${BASE_URL}/persons`;

export const createPersona = async (data: any) => {
    const url = `${URL_COMPONENT}`;
    return await create(data, url);
}

export const updatePersona = async (data: any, id: number) => {
    const url = `${URL_COMPONENT}/${id}`;
    return helpFetch().put(url, {data}).then((resp: ResponseRequest) => {
        return resp;
    })
        .catch(err => console.log(err));
}

export const getPersonaList = async (params: any)=> {
    let url = `${URL_COMPONENT}?`;
    const keys = Object.keys(params);

    for (const key of keys) {
        if (params[key].length > 0) {
            url += `${key}=${params[key]}&`;
        }
    }
    url = url.slice(0, -1);
    return helpFetch().get(url).then((resp: ResponseRequest) => {
        return resp;
    })
        .catch(err => console.log(err));
}

export const deletePersona = async (id: number) => {
    const url = `${URL_COMPONENT}/${id}`
    return helpFetch().del(url).then((resp: ResponseRequest) => {
        return resp;
    })
        .catch(err => console.log(err));
}

import {IPersonaCreate} from "./interfaces/Persona";
import {ResponseRequest} from "../../core/models/ResponseRequest";
import {createPersona, deletePersona, getPersonaList, updatePersona} from "./api/personas.service";
import {personas} from "./personaSlice";
import moment from "moment";

export const startCreatePersona = (data: IPersonaCreate) => {
    return async () => {
        return await createPersona(data).then((resp: ResponseRequest) => {
            if (resp !== undefined && resp.status === 201) {
                return resp.data;
            }
        })
    }
}

export const startUpdatePersona = (data: IPersonaCreate, id: number) => {
    return async () => {
        // @ts-ignore
        return await updatePersona(data, id).then((resp: ResponseRequest) => {
            if (resp !== undefined && resp.status === 200) {
                return resp.data;
            }
        })
    }
}

export const startGetPersonaList = (params: any) => {
    return async (dispatch: any) => {
        // @ts-ignore
        return await getPersonaList(params).then((resp: ResponseRequest) => {
            if (resp !== undefined && resp.status === 200) {
                dispatch(personas(resp.data))
                return resp;
            }
        })
    }
}


export const startDeletePersona = (id: number) => {
    return async () => {
        // @ts-ignore
        return await deletePersona(id).then((resp: ResponseRequest) => {
            if (resp !== undefined && resp.status === 201) {
                return resp.data;
            }
        })
    }
}


export interface IPersona {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
}

export interface IPersonaCreate {
    firstName: string;
    lastName: string;
    email: string;
}

import {createSlice} from "@reduxjs/toolkit";
import {IPersona} from "./interfaces/Persona";

const initPersonasState: IPersona[] = [{
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
}]

const initialState = {
    personaList: initPersonasState,
}

export const personaSlice = createSlice({
    name: 'Personas',
    initialState,
    reducers: {
        personas: (state, {payload}) => {
            state.personaList = payload
        },
    }
})

export const { personas } = personaSlice.actions;

import {Suspense, useMemo} from "react";
import AppRoutes from "../../routes/AppRoutes";
import {UseCheckAuth} from "../../core/hooks";
import {StatusEnum} from "../../store/auth/enum/status.enum";
import {HeaderComponent} from "../../UI/HeaderComponent";
import background from '../../assets/img/buyer-personas.jpg';


const Content = () => {
    const {status} = UseCheckAuth();
    const isAuthenticated = useMemo(() => status === StatusEnum.AUTHENTICATED, [status]);

    return (
        <>
            <main className="main_container" style={{backgroundImage: !isAuthenticated ? `url(${background})` : ''}}>
                <div className="header_container">
                    <HeaderComponent />
                </div>

                <div className="body_container">
                    <Suspense fallback={'Loading...'}>
                        <AppRoutes />
                    </Suspense>
                </div>

            </main>
        </>
    )
}

export default Content;
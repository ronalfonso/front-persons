import {useState} from 'react';
import {GeneralContext} from './GeneralContext';


export const GeneralProvider = ({ children }: any) => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [loading, setLoading, ] = useState<boolean>(false);
    const [isUnauthorized, setIsUnauthorized] = useState(false);
    const [homeSelected, setHomeSelected] = useState(null);
    const [itemEdit, setItemEdit] = useState<any | null>(null);
    const [idItem, setIdItem] = useState<number | null>(null);
    const [openPersonaDialog, setOpenPersonaDialog] = useState({action: '', state: false});
    const [recharge, setRecharge] = useState(false);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const data = {
        isLoading, setIsLoading,
        isUnauthorized, setIsUnauthorized,
        homeSelected, setHomeSelected,
        itemEdit, setItemEdit,
        loading, setLoading,
        openPersonaDialog, setOpenPersonaDialog,
        recharge, setRecharge,
        page, setPage,
        rowsPerPage, setRowsPerPage,
        idItem, setIdItem,
    }

    return (
        <GeneralContext.Provider value={ data } >
            { children }
        </GeneralContext.Provider>
    )
}
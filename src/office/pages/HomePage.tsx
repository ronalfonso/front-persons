import {useContext, useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../store/store";
import {GeneralContext} from "../../contexts";
import {
    Box, Button,
    Card, CardActions,
    CardContent,
    CardHeader,
    IconButton,
    TextField
} from "@mui/material";
import {Add, CleaningServices, Search} from "@mui/icons-material";
import {TableComponent} from "../../core/components/table/TableComponent";
import {PersonaDialog} from "../../core/components/PersonaDialog/PersonaDialog";
import {startGetPersonaList} from "../../store/personas";
import {IPersona} from "../../store/personas/interfaces/Persona";

const HomePage = () => {
    const { setOpenPersonaDialog, setRecharge, recharge } = useContext<any>(GeneralContext);
    const {personaList} = useAppSelector((state) => state.personas)
    const dispatch = useAppDispatch();
    const [rows, setRows] = useState<IPersona[]>([]);
    const [params, setParams] = useState({
        page: 0,
        size: 10,
        name: '',
    })



    const handleClickOpen = () => {
        setOpenPersonaDialog({action: 'crear', state: true});
    };

    const heads = [
        {nameData: 'id', value: 'id'},
        {nameData: 'firstName', value: 'nombre'},
        {nameData: 'lastName', value: 'apellido'},
        {nameData: 'email', value: 'correo'},
    ]

    const handleSearch = () => {
        _getPersonaList();
    }

    const handleClean = () => {
        setParams({
            page: 0, size: 10, name: ''
        })
        _getPersonaList({
            page: 0, size: 10, name: ''
        });
    }

    const _getPersonaList = (paramsArg?: any) => {
        setRecharge(true);
        dispatch(startGetPersonaList(paramsArg !== undefined ? paramsArg : params))
        setRecharge(false);
    }


    useEffect(() => {
        const promises = [_getPersonaList()];
        Promise.all(promises)
    },[])

    useEffect(() => {
        if (recharge === true)
        _getPersonaList();
    }, [recharge]);

    useEffect(() => {
        setRows(personaList);
    }, [personaList])


    return (
        <>
            <div className={'content'}>
                <Card className="filters" elevation={12}>
                    <CardHeader sx={{p: 0}} title={'Filtros'}/>
                    <CardContent>
                        <Box>
                            <TextField
                                id="outlined-required"
                                label="Nombre"
                                name={'name'}
                                value={params.name}
                                onChange={(e) => {
                                    setParams({...params, name: e.target.value})
                                }}
                                placeholder={'Nombre'}
                                size={'small'}
                            />

                        </Box>

                    </CardContent>
                    <CardActions sx={{display: 'flex', justifyContent: 'space-between'}}>
                        <Button variant={'text'}
                                color={'secondary'}
                                onClick={handleClean}
                        ><CleaningServices/>Limpiar</Button>
                        <Button variant={'contained'}
                                onClick={handleSearch}
                        ><Search/>Buscar</Button>
                    </CardActions>
                </Card>

                <Card className="table_personas" elevation={12}>
                    <CardHeader sx={{p: 0}} title={'Personas'}
                                action={
                                    <IconButton onClick={handleClickOpen}>
                                        <Add/>
                                    </IconButton>
                                }
                    />
                    <CardContent>
                        <TableComponent
                            heads={heads}
                            rows={rows}
                            canEdit={true}
                            canDelete={true}
                        />
                    </CardContent>
                </Card>
            </div>

            <PersonaDialog/>

        </>
    )
}

export default HomePage;

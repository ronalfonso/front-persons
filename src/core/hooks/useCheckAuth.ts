import {useContext, useEffect, useMemo} from 'react';
import {GeneralContext} from '../../contexts';
import {useAppSelector} from "../../store/store";
import {StatusEnum} from "../../store/auth/enum/status.enum";

export const UseCheckAuth = () => {
    const { setIsLoading } = useContext<any>(GeneralContext);
    const {user, status} = useAppSelector((state) => state.auth);
    const isAuthenticated = useMemo(() => status === StatusEnum.AUTHENTICATED, [status]);

    const authChecking = async () => {
        try {
            setIsLoading(true)
            const token = (localStorage.getItem('token')) || '';
            if (token.length > 0) {
                saveLoginUser();
            }
        } catch (e) {
            console.error(e)
            setIsLoading(false)
        }
    }

    const saveLoginUser = async () => {
        try {
            // Aqui se guarda el usuario en una base de datos en el navegador para asegurar los datos
            localStorage.setItem('user', JSON.stringify(user))
            setIsLoading(false)
        } catch (err) {
            console.error(err);
            setIsLoading(false)
        }

    }

    useEffect(() => {
        authChecking();
    }, [isAuthenticated]);

    return {
        status
    }
}

import React, {ChangeEvent, useContext, useEffect, useState, MouseEvent} from "react";
import {
    IconButton,
    Paper,
    styled,
    Table,
    TableBody,
    TableCell,
    tableCellClasses,
    TableContainer, TableFooter,
    TableHead, TablePagination,
    TableRow
} from "@mui/material";
import {capitalizeLabel} from "../../utils/handle-lables";
import styles from '../../../styles/style.module.scss';
import {Delete, Edit} from "@mui/icons-material";
import {GeneralContext} from "../../../contexts";
import {TablePaginationActions} from "./TablePaginationActions";


const StyledTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: styles.colorPrimary,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({theme}) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));


export const TableComponent = ({rows, heads, canEdit, canDelete}: any) => {
    const { setOpenPersonaDialog, page, setPage, setItemEdit,
        rowsPerPage, setRowsPerPage, setIdItem, } = useContext<any>(GeneralContext);
    const [orderedRows, setOrderedRows] = useState([]);

    const ordenarPropiedades = (rows: any, heads: any) => {
        const orderedRows = rows.map((row: any) => {
            const orderedRow = {};
            heads.forEach((head: any) => {
                // @ts-ignore
                orderedRow[head.nameData] = row[head.nameData];
            });
            return orderedRow;
        });
        return orderedRows;
    };

    const handleEdit = (row: any) => {
        console.log("Edición en progreso para el registro:", row);
        setItemEdit(row);
        setTimeout(() => {
            setOpenPersonaDialog({action: 'edición', state: true})
        },100)
    };

    const handleDelete = (rowId: any) => {
        console.log("Eliminación en progreso para el registro:", rowId);
        setOpenPersonaDialog({action: 'borrar', state: true})
        setIdItem(rowId);
    };


    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleChangePage = (
        event: MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    useEffect(() => {
        const orderedRows = ordenarPropiedades(rows, heads);
        setOrderedRows(orderedRows);
    }, [rows])

    return (
        <TableContainer component={Paper} sx={{maxHeight: 440}}>
            <Table stickyHeader sx={{minWidth: 700}}>
                <TableHead>
                    {
                        heads.map((head: any, index: number) =>
                            <StyledTableCell
                                key={index}>{capitalizeLabel(head.value)}</StyledTableCell>)
                    }
                    {
                        (canDelete !== false || canEdit !== false) &&
                        <StyledTableCell>Opciones</StyledTableCell>
                    }
                </TableHead>
                <TableBody>
                    {
                        orderedRows.map((row, index) => (
                            <StyledTableRow key={index}>
                                {
                                    heads.map((title: any, index: number) => (
                                        <StyledTableCell key={index} scope="row">
                                            {row[title.nameData]}
                                        </StyledTableCell>
                                    ))
                                }
                                {
                                    (canDelete !== false || canEdit !== false) &&
                                    <StyledTableCell align="center" sx={{display:'flex', justifyContent: 'space-around'}}>
                                        <IconButton sx={{backgroundColor: styles.colorSecondary, color: 'white'}}
                                                    onClick={() => handleEdit(row)}
                                        >
                                            <Edit />
                                        </IconButton>
                                        <IconButton sx={{backgroundColor: styles.colorDanger, color: 'white'}}
                                                    onClick={() => handleDelete(row['id'])}
                                        >
                                            <Delete />
                                        </IconButton>
                                    </StyledTableCell>
                                }
                            </StyledTableRow>
                        ))
                    }
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25]}
                            colSpan={3}
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: {
                                    'aria-label': 'Personas por página',
                                },
                                native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    )
}

import {useContext, useEffect, useState} from "react";
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
    Typography
} from "@mui/material"
import {GeneralContext} from "../../../contexts";
import {capitalizeLabel} from "../../utils/handle-lables";
import * as Yup from "yup";
import {useFormik} from "formik";
import {useAppDispatch} from "../../../store/store";
import {startCreatePersona, startDeletePersona, startUpdatePersona} from "../../../store/personas";

export const PersonaDialog = () => {
    const {
        openPersonaDialog,
        setOpenPersonaDialog,
        setRecharge,
        idItem,
        setIdItem,
        itemEdit,
    } = useContext<any>(GeneralContext);
    const dispatch = useAppDispatch();
    const [title, setTitle] = useState('')

    const yupSchema = Yup.object({
        firstName: Yup.string()
            .required("El nombre es obligatorio")
            .min(4, "El nombre debe tener al menos 4 caracteres")
            .max(25, "El nombre no debe tener más de 25 caracteres"),
        lastName: Yup.string()
            .required("El apellido es obligatorio")
            .min(4, "El apellido debe tener al menos 4 caracteres")
            .max(25, "El apellido no debe tener más de 25 caracteres"),
        email: Yup.string()
            .required("El email es obligatorio")
            .email("Debe ser un email valido"),
    });

    const {values,
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isValid,
        resetForm,
        } = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            email: "",
        },
        validationSchema: yupSchema,
        onSubmit: (values) => {
            if (openPersonaDialog.action === 'crear') {
                dispatch(startCreatePersona(values))
                resetForm();
            } else {
                dispatch(startUpdatePersona(values, itemEdit.id))
            }
            handleClose();
            setRecharge(true);
        },
    });

    const handleClose = () => {
        setOpenPersonaDialog({action: '', state: false});
    };

    const confirmDelete = () => {
        dispatch(startDeletePersona(idItem))
        handleClose();
        setTimeout(() => {
            setIdItem(null)
        }, 500)
        setRecharge(true)
    }

    useEffect(() => {
        setTitle(openPersonaDialog.action)
    }, [openPersonaDialog])


    return (
        <>
            <Dialog
                fullWidth
                maxWidth={"md"}
                open={openPersonaDialog.state}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle>
                    Datos generales: <Typography>{capitalizeLabel(title)}</Typography>
                </DialogTitle>
                <DialogContent>
                    {
                        openPersonaDialog.action !== 'borrar' ?
                            <form onSubmit={handleSubmit} className={'form_personas'}>
                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="firstName"
                                        label="Nombre"
                                        value={values.firstName}
                                        helperText={errors.firstName}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        fullWidth
                                        size={'small'}
                                    />
                                </Box>
                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="lastName"
                                        label="Apellido"
                                        value={values.lastName}
                                        helperText={errors.lastName}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        fullWidth
                                        size={'small'}
                                    />
                                </Box>

                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="email"
                                        label="Email"
                                        value={values.email}
                                        helperText={errors.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        fullWidth
                                        size={'small'}
                                    />
                                </Box>

                                {
                                    openPersonaDialog.action === 'crear' ?
                                        <Button sx={{height: 40}} variant="contained" type="submit" disabled={!isValid}>
                                            Enviar
                                        </Button>
                                        :
                                        <Button sx={{height: 40}} variant="contained" type="submit" disabled={!isValid}>
                                            Modificar
                                        </Button>
                                }

                            </form>
                            :
                            <Typography>
                                ¿Está seguro de que quiere eliminar el registro? Luego de esta acción no se podrán
                                recuperar los datos. </Typography>

                    }

                </DialogContent>
                <DialogActions>
                    <Button sx={{height: 40}} color={'error'} onClick={handleClose}>
                        Cancelar
                    </Button>
                    {
                        openPersonaDialog.action === 'borrar' &&
                        <Button sx={{height: 40}} onClick={confirmDelete}>
                            Confirmar
                        </Button>
                    }

                </DialogActions>
            </Dialog>
        </>
    )
}

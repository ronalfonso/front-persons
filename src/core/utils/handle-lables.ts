

export const capitalizeLabel = (label: string) => {
    return label.charAt(0).toUpperCase() + label.slice(1).toLowerCase();
}

export const uppercaseLabel = (label: string) => {
    return label.toUpperCase();
}

export const lowercaseLabel = (label: string) => {
    return label.toLowerCase();
}
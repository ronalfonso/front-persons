import {lazy} from "react";

const SCREENS = {
    LOGIN: lazy( () => import(/*webpackChunkName: "Login" */'./../auth/pages/LoginPage')),
    HOME: lazy(() => import(/*webpackChunkName: "Home" */'./../office/pages/HomePage'))
}

const routes = [
    {
        name: 'Home',
        path: '/',
        component: SCREENS.HOME,
        exact: true,
        requiresAuth: true,
    },
    {
        name: 'Login',
        path: '/login',
        component: SCREENS.LOGIN,
        exact: true,
        requiresAuth: false,
    },
]

export default routes;
import {Navigate, Route, Routes} from "react-router-dom";
import routes from "./routes";
import {UseCheckAuth} from "../core/hooks";
import {useMemo} from "react";
import {StatusEnum} from "../store/auth/enum/status.enum";


const AppRoutes = () => {
    const {status} = UseCheckAuth();
    const isAuthenticated = useMemo(() => status === StatusEnum.AUTHENTICATED, [status]);

    return(
        <Routes>
            {
                routes.map((route, index) => {
                    if (route.requiresAuth && !isAuthenticated) {
                        return (
                            <Route
                                key={index}
                                path={route.path}
                                element={<Navigate to={'/login'} />}
                            />
                        )
                    }

                    if (route.path === '/login' && isAuthenticated) {
                        return (
                            <Route
                                key={index}
                                path={route.path}
                                element={<Navigate to="/" />}
                            />
                        )
                    }

                    return (
                        <Route
                            key={index}
                            path={route.path}
                            element={<route.component />}
                        />
                    )
                })
            }
        </Routes>
    )
}

export default AppRoutes;